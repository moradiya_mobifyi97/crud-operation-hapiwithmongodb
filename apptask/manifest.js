const Path = require('path');

module.exports = {
  server: {
    port: 8000,
    routes: {
      files: {
        relativeTo: Path.join(__dirname, 'public')
      }
    }
  },
  register: {
    plugins: [
		{
			plugin: require('./src/auth')
		},
      {
        plugin: require('./src/employee'),
        routes: {
          prefix: '/employee'
        }
	  }
    ]
  }
};