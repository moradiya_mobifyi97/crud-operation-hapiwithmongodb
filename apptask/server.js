'use strict';
const mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/employeeapi",{useNewUrlParser:true},(err) => {
   if(!err){
       console.log("Mongodb connected");
   }else{
       console.log("Mongodb conntection faild"); 
   }
})

const Glue = require('glue');
const manifest = require('./manifest');

const options = {
  relativeTo: __dirname
};

const startServer = async function () {
  try {
    const server = await Glue.compose(manifest, options);
    await server.start();
    console.log(`server started on port ${manifest.server.port}`);
  }
  catch (err) {
    console.error(err);
    process.exit(1);
  }
};

startServer();