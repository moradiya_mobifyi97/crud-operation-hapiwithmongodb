const Employee = require("../models/employee");

exports.list = (req, h) => {
    return Employee.find()
            .exec()
            .then((doc) => {
                console.log("doc", doc);
                return { doc: doc }
            })
            .catch((err) => {
                return { err: err }
            })
}

exports.create = (req, h) => {
    const employeeData = {
        firstname:req.payload.firstname,
        lastname:req.payload.lastname,
        phone:req.payload.phone,
        email:req.payload.email
    };

    return Employee.create(employeeData).then((result) =>{
        return {message:"Employee create Successfully", result: result}
    }).catch((err) => {
        return { err:err }
    })
  }

exports.get = (req, h) => {
    return Employee.findById(req.params.id)
            .exec()
            .then((doc) => {
                console.log("doc", doc)
                if(!doc) return { message: "Employee not Found"}
                return { doc: doc }
            })
            .catch((err) => {
                return { err: err }
            })
}

exports.update = (req, h) => {
    return Employee.findById(req.params.id)
        .exec().then((doc) => {
            if(!doc) return { message:"Employee not Found" }
            doc.firstname = req.payload.firstname,
            doc.lastname = req.payload.lastname,
            doc.phone = req.payload.phone,
            doc.email = req.payload.email

            doc.save();
        }).then((result) =>{
            return { message: "employee data update successfully"}
        }).catch((err) => {
            return { err:err }
        });
}

exports.delete = (req, h) =>{
    return Employee.findById(req.params.id)
        .exec(function (err, doc){
            if(err) return {err:err};
            if(!doc) return { message:"Employee not Found" }

            doc.remove(function(err){
                if(err) return {err:err}
                return { message:"Employee data delete successfully"}
            });
        });
}