
const employeeController = require('./service');
const Joi = require('joi');

exports.register = (server) => {
server.route({
    method:"GET",
    path:'/employee',
    config:{
        auth:'simple', // Authorization 
        handler:employeeController.list
      }
    
});

server.route({
    method:"POST",
    path:'/employee',
    config:{
        validate:{
            payload:{
                firstname: Joi.string().min(3).max(50).required(),
                lastname: Joi.string().min(3).max(50).required(),
                phone: Joi.number().required(),
                email: Joi.string().email().required()
            },
            failAction:(request, h, error) => {
                throw error;
            },
        },
        handler:employeeController.create
    }, 
    
});

server.route({
    method:"GET",
    path:'/employee/{id}',
    handler:employeeController.get
});

server.route({
    method:"PUT",
    path:'/employee/{id}',
    config:{
        validate:{
            payload:{
                firstname: Joi.string().min(3).max(50).required(),
                lastname: Joi.string().min(3).max(50).required(),
                phone: Joi.number().required(),
                email: Joi.string().email().required()
            },
            failAction:(request, h, error) => {
                throw error;
            },
        },
        handler:employeeController.update
    },
});

server.route({
    method:"DELETE",
    path:'/employee/{id}',
    handler:employeeController.delete
});

};

exports.pkg = {
  name: "employeecontrollers"
};